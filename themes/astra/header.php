<?php
/**
 * The header for Astra Theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Astra
 * @since 1.0.0
 */
 $sitehome = get_site_url() ;
 ?>
 <link rel='stylesheet' href='<?php echo $sitehome; ?>/wp-content/themes/suros/style.css' type='text/css' media='all' />
 <link rel='stylesheet' href='<?php echo $sitehome; ?>/wp-content/themes/suros/css/neutral.css' type='text/css' media='all' />
 <script src='<?php echo $sitehome; ?>/wp-content/themes/suros//scripts/navbar.js' /></script>
 <div class="main-header-bar-wrap">

 <?php
 $sitehome = get_site_url() ;
 include $_SERVER['DOCUMENT_ROOT'].'/wp-content/themes/suros/complements/header-nav.php';
 echo $sitehome.'/wp-content/themes/suros/complements/header-nav.php';
  ?>

 </div>
