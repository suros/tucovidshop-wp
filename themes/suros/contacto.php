<?php
//particular
//empresa
$navbar = "";
include 'header.php';
?>
<?php /* Template Name: contacto */;?>


<div class="bg-white py-32 px-4 overflow-hidden sm:px-6 lg:px-8">
    <div class="relative max-w-xl mx-auto">
        <svg class="absolute left-full transform translate-x-1/2" width="404" height="404" fill="none"
             viewBox="0 0 404 404" aria-hidden="true">
            <defs>
                <pattern id="85737c0e-0916-41d7-917f-596dc7edfa27" x="0" y="0" width="20" height="20"
                         patternUnits="userSpaceOnUse">
                    <rect x="0" y="0" width="4" height="4" class="text-gray-200" fill="currentColor"></rect>
                </pattern>
            </defs>
            <rect width="404" height="404" fill="url(#85737c0e-0916-41d7-917f-596dc7edfa27)"></rect>
        </svg>
        <svg class="absolute right-full bottom-0 transform -translate-x-1/2" width="404" height="404" fill="none"
             viewBox="0 0 404 404" aria-hidden="true">
            <defs>
                <pattern id="85737c0e-0916-41d7-917f-596dc7edfa27" x="0" y="0" width="20" height="20"
                         patternUnits="userSpaceOnUse">
                    <rect x="0" y="0" width="4" height="4" class="text-gray-200" fill="currentColor"></rect>
                </pattern>
            </defs>
            <rect width="404" height="404" fill="url(#85737c0e-0916-41d7-917f-596dc7edfa27)"></rect>
        </svg>
        <div class="text-center">
            <h1 class="text-3xl font-extrabold tracking-tight text-gray-900 sm:text-4xl">
                Contacto
            </h1>
            <p class="mt-4 text-lg leading-6 text-gray-500">
                Nullam risus blandit ac aliquam justo ipsum. Quam mauris volutpat massa dictumst amet. Sapien tortor
                lacus arcu.
            </p>
        </div>
        <div class="mt-12">
          <?php  echo do_shortcode( '[contact-form-7 id="15" title="Contacto"]' );  ?>




<?php /*

          <div class="bg-white py-32 px-4 overflow-hidden sm:px-6 lg:px-8">
              <div class="relative max-w-xl mx-auto">
                  <svg class="absolute left-full transform translate-x-1/2" width="404" height="404" fill="none"
                       viewBox="0 0 404 404" aria-hidden="true">
                      <defs>
                          <pattern id="85737c0e-0916-41d7-917f-596dc7edfa27" x="0" y="0" width="20" height="20"
                                   patternUnits="userSpaceOnUse">
                              <rect x="0" y="0" width="4" height="4" class="text-gray-200" fill="currentColor"></rect>
                          </pattern>
                      </defs>
                      <rect width="404" height="404" fill="url(#85737c0e-0916-41d7-917f-596dc7edfa27)"></rect>
                  </svg>
                  <svg class="absolute right-full bottom-0 transform -translate-x-1/2" width="404" height="404" fill="none"
                       viewBox="0 0 404 404" aria-hidden="true">
                      <defs>
                          <pattern id="85737c0e-0916-41d7-917f-596dc7edfa27" x="0" y="0" width="20" height="20"
                                   patternUnits="userSpaceOnUse">
                              <rect x="0" y="0" width="4" height="4" class="text-gray-200" fill="currentColor"></rect>
                          </pattern>
                      </defs>
                      <rect width="404" height="404" fill="url(#85737c0e-0916-41d7-917f-596dc7edfa27)"></rect>
                  </svg>
                  <div class="text-center">
                      <h2 class="text-3xl font-extrabold tracking-tight text-gray-900 sm:text-4xl">
                          Contacto
                      </h2>
                      <p class="mt-4 text-lg leading-6 text-gray-500">
                          Nullam risus blandit ac aliquam justo ipsum. Quam mauris volutpat massa dictumst amet. Sapien tortor
                          lacus arcu.
                      </p>
                  </div>
                  <div class="mt-12 wpcf7" role="form" id="wpcf7-f15-o1" lang="es-ES" dir="ltr">
                      <form action="/contacto/#wpcf7-f15-o1"
                            method="post"
                            novalidate="novalidate"
                            data-status="init"
                            class="grid grid-cols-1 gap-y-6 sm:grid-cols-2 sm:gap-x-8 wpcf7-form init">
                          <div style="display: none;">
                              <input type="hidden" name="_wpcf7" value="15">
                              <input type="hidden" name="_wpcf7_version" value="5.4">
                              <input type="hidden" name="_wpcf7_locale" value="es_ES">
                              <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f15-o1">
                              <input type="hidden" name="_wpcf7_container_post" value="0">
                              <input type="hidden" name="_wpcf7_posted_data_hash" value="">
                          </div>
                          <div class="sm:col-span-2">
                              <label for="name" class="block text-sm font-medium text-gray-700">Nombre</label>
                              <div class="mt-1">
                                  <input type="text"
                                         aria-required="true"
                                         aria-invalid="false"
                                         name="your-name"
                                         value=""
                                         size="40"
                                         id="name"
                                         autocomplete="name"
                                         class="py-3 px-4 block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 rounded-md wpcf7-form-control wpcf7-text wpcf7-validates-as-required">
                              </div>
                          </div>
                          <div class="sm:col-span-2">
                              <label for="Empresa" class="block text-sm font-medium text-gray-700">Empresa</label>
                              <div class="mt-1">
                                  <input type="text"
                                         name="Empresa"
                                         value=""
                                         size="40"
                                         aria-required="true"
                                         aria-invalid="false"
                                         id="Empresa"
                                         autocomplete="Empresa"
                                         class="py-3 px-4 block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 rounded-md wpcf7-form-control wpcf7-text wpcf7-validates-as-required">
                              </div>
                              <div class="mt-1 text-gray-400 text-xs">Solo si eres empresa</div>
                          </div>
                          <div class="sm:col-span-2">
                              <label for="email" class="block text-sm font-medium text-gray-700">Email</label>
                              <div class="mt-1">
                                  <input name="your-email"
                                         value=""
                                         size="40"
                                         id="email"
                                         aria-required="true"
                                         aria-invalid="false"
                                         type="email" autocomplete="email"
                                         class="py-3 px-4 block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 rounded-md wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email">
                              </div>
                          </div>
                          <div class="sm:col-span-2">
                              <label for="Telefono" class="block text-sm font-medium text-gray-700">Telefono</label>
                              <div class="mt-1 relative rounded-md shadow-sm">
                                  <input type="text"
                                         name="Telefono"
                                         value=""
                                         size="40"
                                         aria-required="true"
                                         aria-invalid="false"
                                         autocomplete="Telefono"
                                         class="py-3 px-4 block w-full focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 rounded-md wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel"
                                         placeholder="+34 123 456 789">
                              </div>
                          </div>
                          <div class="sm:col-span-2">
                              <label for="your-message" class="block text-sm font-medium text-gray-700">Mensaje</label>
                              <div class="mt-1">
                                  <textarea id="your-message" name="your-message" rows="4"
                                            class="py-3 px-4 block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 rounded-md wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required"></textarea>
                              </div>
                          </div>
                          <div class="sm:col-span-2">
                              <div class="flex items-start">

                                  <div class="ml-3">
                                      <p class="text-base text-gray-500">
                                          Al enviar este formulario, acepto la:
                                          <!-- space -->
                                          <a href="#" class="font-medium text-gray-700 underline">Política de privacidad</a>
                                          <!-- space -->
                                          y la
                                          <!-- space -->
                                          <a href="#" class="font-medium text-gray-700 underline">Política de cookies</a>.
                                      </p>
                                  </div>
                              </div>
                          </div>
                          <div class="sm:col-span-2">
                              <button type="submit"
                                      class="wpcf7-form-control wpcf7-submit w-full inline-flex items-center justify-center px-6 py-3 border border-transparent rounded-md shadow-sm text-base font-medium text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                      value="Enviar"><span
                                      class="ajax-loader"></span>
                                  Enviar
                              </button>
                              <div class="wpcf7-response-output" aria-hidden="true"></div>
                          </div>
                      </form>
                  </div>
              </div>
          </div>


 */?>







        </div>
    </div>
</div>
</x-layout>




<?php get_footer(); ?>
