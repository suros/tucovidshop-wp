<?php
include 'head.php';
?>
<?php /* Template Name: e-domicilio */;?>
<x-layout>
<div>
<div class="bg-white pt-8">
<main>
<div class="pt-12 sm:pt-16 lg:pt-20">
<?php echo titleh1(); ?>
<div class="bg-white py-32 px-4 overflow-hidden sm:px-6 lg:px-8">
    <div class="relative max-w-xl mx-auto">

<svg class="absolute left-full transform translate-x-1/2" width="404" height="404" fill="none"
     viewBox="0 0 404 404" aria-hidden="true">
    <defs>
        <pattern id="85737c0e-0916-41d7-917f-596dc7edfa27" x="0" y="0" width="20" height="20"
                 patternUnits="userSpaceOnUse">
            <rect x="0" y="0" width="4" height="4" class="text-gray-200" fill="currentColor"></rect>
        </pattern>
    </defs>
    <rect width="404" height="404" fill="url(#85737c0e-0916-41d7-917f-596dc7edfa27)"></rect>
</svg>
<svg class="absolute right-full bottom-0 transform -translate-x-1/2" width="404" height="404" fill="none"
     viewBox="0 0 404 404" aria-hidden="true">
    <defs>
        <pattern id="85737c0e-0916-41d7-917f-596dc7edfa27" x="0" y="0" width="20" height="20"
                 patternUnits="userSpaceOnUse">
            <rect x="0" y="0" width="4" height="4" class="text-gray-200" fill="currentColor"></rect>
        </pattern>
    </defs>
    <rect width="404" height="404" fill="url(#85737c0e-0916-41d7-917f-596dc7edfa27)"></rect>
</svg>
<?php
echo do_shortcode('[booking type=1 nummonths=2]');
?>
</div>
</div>




</div>
</main>
</div>

</div>
</x-layout>

<?php get_footer(); ?>
