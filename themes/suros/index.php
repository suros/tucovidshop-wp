<?php
include 'header.php';
?>
<?php /* Template Name: e-domicilio */;?>
<x-layout>
<div>
<div class="bg-white pt-8">
<main>
<div class="pt-12 sm:pt-16 lg:pt-20">
<?php echo the_title(); ?>

<?php
echo get_the_content();
?>



</div>
</main>
</div>

</div>
</x-layout>

<?php get_footer(); ?>
