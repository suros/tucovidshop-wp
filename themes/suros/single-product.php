<?php
$doarproduct = 'doarproduct';
global $post;
      $terms = get_the_terms( $post->ID, 'product_cat' );
      $nterms = get_the_terms( $post->ID, 'product_tag'  );
      foreach ($terms  as $term  ) {
          $product_cat_id = $term->term_id;
          $product_cat_name = $term->name;
          break;
      }





//$spvar = $navbar;



switch($product_cat_name)
{
    case 'Empresa';
    case 'empresa';
    case 'Maquinas';
    case 'Antígenos';
    case 'anticuerpos';
    case 'pcr-nasofaringeos';
    case 'pcr-saliva';
        $spvar = 'empresa';
    break;
    default;
        $spvar = 'particular';
    break;
}


$navbar = $spvar;
include 'header.php';


?>
<x-layout>
    <section class="text-gray-600 body-font overflow-hidden h-screen flex justify-center items-center">
        <div class="container px-5 py-24 mx-auto">
            <div class="lg:w-4/5 mx-auto flex flex-wrap">
              <div class="lg:w-1/2 w-full lg:h-auto h-64 object-cover object-center rounded">
                <img alt="ecommerce" class="lg:w-1/2 w-full lg:h-auto h-64 object-cover object-center rounded"
                     src="<?php echo get_the_post_thumbnail_url();?>">
                   </div>
                <div class="lg:w-1/2 w-full lg:pl-10 lg:py-6 mt-6 lg:mt-0">
                    <div class="text-sm title-font text-gray-500 tracking-widest"><?php  echo get_current_product_category(); ?></div>
                    <h1 class="text-gray-900 text-3xl title-font font-medium mb-1"><?php echo the_title();?></h1>
                    <div class="flex mb-4">
                        <a class="text-gray-500">
                            <svg fill="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                 class="w-5 h-5"
                                 viewBox="0 0 24 24">
                                <path d="M18 2h-3a5 5 0 00-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 011-1h3z"></path>
                            </svg>
                        </a>
                        <a class="text-gray-500">
                            <svg fill="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                 class="w-5 h-5"
                                 viewBox="0 0 24 24">
                                <path
                                    d="M23 3a10.9 10.9 0 01-3.14 1.53 4.48 4.48 0 00-7.86 3v1A10.66 10.66 0 013 4s-4 9 5 13a11.64 11.64 0 01-7 2c9 5 20 0 20-11.5a4.5 4.5 0 00-.08-.83A7.72 7.72 0 0023 3z"></path>
                            </svg>
                        </a>
                        <a class="text-gray-500">
                            <svg fill="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                 class="w-5 h-5"
                                 viewBox="0 0 24 24">
                                <path
                                    d="M21 11.5a8.38 8.38 0 01-.9 3.8 8.5 8.5 0 01-7.6 4.7 8.38 8.38 0 01-3.8-.9L3 21l1.9-5.7a8.38 8.38 0 01-.9-3.8 8.5 8.5 0 014.7-7.6 8.38 8.38 0 013.8-.9h.5a8.48 8.48 0 018 8v.5z"></path>
                            </svg>
                        </a>
                        </span>
                    </div>
                    <p class="leading-relaxed">
                      <?php the_content();?></p>

<?php if ($navbar == 'particular'): ?>

<?php elseif ($navbar == 'empresa'): ?>
<div class="warningprod"><b>NOTA IMPORTANTE</b>: Este es un producto catalogado como de “Diagnóstico In Vitro”, no es un producto de “autodiagnóstico”, por tanto <b>no puede ser vendido a personas particulares</b>, sino a Hospitales, Centros de Salud, Clínicas, Laboratorios, Residencias de Ancianos, Servicios Públicos, Mutuas de Prevención de Riesgos o Empresas, siempre que se cumplan las directrices establecidas por la orden Ministerial publicada en el <a class="link" target="_blank" rel="noopener" title="descargar PDF" href="https://www.boe.es/boe/dias/2020/04/14/pdfs/BOE-A-2020-4442.pdf">BOE de 14 de abril de 2020</a>, en el que se establece que “se limita la realización de las pruebas diagnósticas para la detección del COVID-19 a aquellos casos en los que exista una prescripción previa por un facultativo y se ajusten a criterios establecidos por la autoridad sanitaria competente”. </br><b>Si usted opta por realizar esta compra está aceptando su uso como profesional</b>.</div>

<?php else: ?>
<?php endif ?>



                    <div class="my-6 border-t-2 border-gray-300"></div>
                    <div class="flex">

                        <span class="title-font font-medium text-2xl text-gray-900">
                          <?php
                          echo add_price_widget() . ' €';
                          /*if (add_price_widget() > 0){
                            echo add_price_widget() . ' €';}
                            else{;}*/
                            ?>
                        </span>
                        <button
                            class="flex ml-auto text-white bg-blue-500 border-0 py-2 px-6 focus:outline-none hover:bg-indigo-600 rounded">
                            <?php echo add_content_after_addtocart();?>
                        </button>

                    </div>
                </div>
            </div>
        </div>
    </section>
</x-layout>
  <?php
  // include 'woocommerce/template-producto.php';
  ?>
  <?php do_action( 'woocommerce_after_single_product' ); ?>




<?php get_footer(); ?>
