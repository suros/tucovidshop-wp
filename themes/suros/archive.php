<?php
include 'head.php';
?>
<?php /* Template Name:  */;?>
<x-layout>
<div>
<div class="bg-white pt-8">
<main>
<div class="pt-12 sm:pt-16 lg:pt-20">
<?php echo titleh1(); ?>

<?php
echo get_the_content();
?>



</div>
</main>
</div>

</div>
</x-layout>

<?php get_footer(); ?>
