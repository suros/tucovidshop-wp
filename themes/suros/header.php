<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;600;700&display=swap" rel="stylesheet">

<link rel='stylesheet' href='<?php echo get_template_directory_uri(); ?>/style.css' type='text/css' media='all' />
<script src='<?php echo get_template_directory_uri(); ?>/scripts/navbar.js' /></script>
<?php /*
<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/images/favicon/apple-57x57.png"/>
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/images/favicon/apple-114x114.png"/>
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/images/favicon/apple-72x72.png"/>
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/images/favicon/apple-144x144.png"/>
<link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/images/favicon/apple-60x60.png"/>
<link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/images/favicon/apple-120x120.png"/>
<link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/images/favicon/apple-76x76.png"/>
<link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/images/favicon/apple-152x152.png"/>
<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/images/favicon/favicon-196x196.png" sizes="196x196"/>
<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/images/favicon/favicon-96x96.png" sizes="96x96'"/>
<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/images/favicon/favicon-32x32.png" sizes="32x32'"/>
<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/images/favicon/favicon-16x16.png" sizes="16x16'"/>
<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/images/favicon/favicon-128.png" sizes="128x128'"/>
<script src="scripts/app.js"></script>
*/
?>
<link rel="apple-touch-icon-precomposed" sizes="300x300" href="<?php echo get_template_directory_uri(); ?>/images/favicon/favicon.png"/>
<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/images/favicon/favicon.png" sizes="300x300"/>

<link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico">
<?php include 'complements/analytics.php';
 wp_head();
 ?>
 <?php
 if ($doarproduct == 'doarproduct'){
   include 'complements/var-s-product.php';
 }else{;}

 if ($navbar == 'particular'): ?>
<link rel='stylesheet' href='<?php echo get_template_directory_uri(); ?>/css/particulares.css' type='text/css' media='all' />
<link rel='stylesheet' href='<?php echo get_template_directory_uri(); ?>/css/responsive-sp.css' type='text/css' media='all' />
 <?php elseif ($navbar == 'empresa'): ?>
<link rel='stylesheet' href='<?php echo get_template_directory_uri(); ?>/css/empresas.css' type='text/css' media='all' />
<link rel='stylesheet' href='<?php echo get_template_directory_uri(); ?>/css/responsive-sp.css' type='text/css' media='all' />

 <?php else: ?>
  <link rel='stylesheet' href='<?php echo get_template_directory_uri(); ?>/css/neutral.css' type='text/css' media='all' />
<?php endif ?>


<link type="text/plain" rel="author" href='<?php echo get_template_directory_uri(); ?>/extras/humans.txt' />

<?php
//jquery slider
/*
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script async src='<?php echo get_template_directory_uri(); ?>/scripts/jquery-3.5.1.min.js'></script>
*/
if ($jqueryslider == 'yes'):?>

<script async src='<?php echo get_template_directory_uri(); ?>/scripts/slider.js'></script>
<?php endif ?>

</head>
<?php
//Barra de navegación si es solo un producto

// Barra de navegación genérica
include 'complements/header-nav.php';
?>
