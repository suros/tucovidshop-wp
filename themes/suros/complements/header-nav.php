<?php
$sitehome = get_site_url() ;
?>

<div class="fixed w-full border-b border-gray-300 bg-white shadow z-50">
<?php
if ($navbar == 'particular'): ?>
        <div class="w-full h-10 bg-blue-400 text-white flex items-center justify-center">
            <div>¿Eres una empresa? Dirígete <a href="<?php echo "$sitehome/empresas"; ?>" class="font-bold"> aquí</a>.</div>
        </div>
<?php elseif ($navbar == 'empresa'): ?>
        <div class="w-full h-10 bg-blue-400 text-white flex items-center justify-center">
            <div>¿Eres un particular? Dirígete <a href="<?php echo "$sitehome/particulares"; ?>" class="font-bold"> aquí</a>.</div>
        </div>
      <?php else: ?>
<?php endif ?>











<header class="text-gray-600 body-font">





<div class="container mx-auto flex flex-wrap p-5 flex-col md:flex-row items-center justify-between">
  <div class="logotipe">
  <span class="flex title-font font-medium items-center justify-center">
    <a href="/"><img src="<?php echo $sitehome.'/wp-content/themes/suros' ?>/images/logo.png" alt="Logo" style="max-width: 40px;display: inline;margin-bottom: 6px;"><span class="text-2xl text-blue-900 font-bold"></span></a>
</span>
<?php if ($navbar == 'particular'): ?>
  <a href="<?php echo $sitehome; ?>" class="flex title-font font-medium items-center mb-4 md:mb-0">
      <span title="volver a la página general"
            class="text-2xl text-blue-900 font-bold">TUCOVIDSHOP | &nbsp; <span
              class="text-2xl text-black font-bold">Particulares</span></span>
  </a></div>
  <nav class="md:ml-auto md:mr-auto flex flex-wrap items-center text-base justify-center">
      <a href="<?php echo "$sitehome/particulares"; ?>" class="mr-5 hover:text-blue-900">Inicio</a>
      <a href="<?php echo "$sitehome/particulares/servicio-domicilio"; ?>" class="mr-5 hover:text-blue-900">Servicio a domicilio</a>
      <a href="<?php echo "$sitehome/particulares/autodiagnostico"; ?>" class="mr-5 hover:text-blue-900">Autodiagnóstico</a>
      <a href="<?php echo "$sitehome/particulares/geles-hidroalcoholicos"; ?>" class="mr-5 hover:text-blue-900">Geles Hidroalcohólicos</a>
      <a href="<?php echo "$sitehome/particulares/mascarillas"; ?>" class="mr-5 hover:text-blue-900">Mascarillas</a>
      <a href="<?php echo "$sitehome/particulares/accesorios"; ?>" class="mr-5 hover:text-blue-900">Accesorios</a>
      <?php echo carrito(); ?>
  </nav>
<?php
// Empresas
elseif ($navbar == 'empresa'): ?>
<a href="<?php echo $sitehome; ?>" class="flex title-font font-medium items-center mb-4 md:mb-0">
    <span title="volver a la página general"
          class="text-2xl text-blue-900 font-bold">TUCOVIDSHOP | &nbsp; <span
            class="text-2xl text-black font-bold">Empresas</span></span>
</a></div>
<nav class="md:ml-auto md:mr-auto flex flex-wrap items-center text-base justify-center">
    <a href="<?php echo "$sitehome/empresas"; ?>" class="mr-5 hover:text-blue-900">Inicio</a>
    <a href="<?php echo "$sitehome/empresas/servicio-a-domicilio"; ?>" class="mr-5 hover:text-blue-900">Servicio a domicilio</a>
    <a href="<?php echo "$sitehome/empresas/maquinas"; ?>" class="mr-5 hover:text-blue-900">Máquinas</a>
    <a href="<?php echo "$sitehome/empresas/antigenos"; ?>" class="mr-5 hover:text-blue-900">Antígenos</a>
    <a href="<?php echo "$sitehome/empresas/pcr"; ?>" class="mr-5 hover:text-blue-900">PCR</a>
    <a href="<?php echo "$sitehome/empresas/anticuerpos"; ?>" class="mr-5 hover:text-blue-900">Anticuerpos</a>
    <?php echo carrito(); ?>
</nav>
<?php else:
//General
  ?>
              <a href="<?php echo $sitehome; ?>" class="flex title-font font-medium items-center mb-4 md:mb-0">
                  <span class="text-2xl text-blue-900 font-bold">TUCOVIDSHOP</span></div>
              </a>
              <nav class="md:ml-auto md:mr-auto flex flex-wrap items-center text-base justify-center">
                  <a href="<?php echo $sitehome; ?>" class="mr-5 hover:text-blue-900">Inicio</a>
                  <a href="<?php echo "$sitehome/empresas"; ?>" class="mr-5 hover:text-blue-900 empresa">Empresas</a>
                  <a href="<?php echo "$sitehome/particulares"; ?>" class="mr-5 hover:text-blue-900 particulares">Particulares</a>
                  <a href="<?php echo "$sitehome/sobre-nosotros"; ?>" class="mr-5 hover:text-blue-900">Sobre Nosotros</a>
                  <?php echo carrito(); ?>
              </nav>
<?php endif ;?>
            <a href="<?php echo "$sitehome/contacto"; ?>"
               class="inline-flex items-center bg-gray-100 border-0 py-2 px-4 focus:outline-none hover:bg-blue-200 rounded text-base mt-4 md:mt-0">
                Contáctanos
                <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                     stroke-width="2" class="w-4 h-4 ml-1" viewBox="0 0 24 24">
                    <path d="M5 12h14M12 5l7 7-7 7"></path>
                </svg>
            </a>


        </div>


<?php// MOVIL ?>

        <div id="mobile-nab" class="navbar-mobile">



<?php if ($navbar == 'particular'): ?>
<ul class="mobile-ulbar-tcs">
<a href="<?php echo "$sitehome/particulares"; ?>" class=""><li>Inicio</li></a>
<a href="<?php echo "$sitehome/particulares/servicio-domicilio"; ?>" class=""><li>Servicio a domicilio</li></a>
<a href="<?php echo "$sitehome/particulares/autodiagnostico"; ?>" class=""><li>Autodiagnóstico</li></a>
<a href="<?php echo "$sitehome/particulares/geles-hidroalcoholicos"; ?>" class=""><li>Geles Hidroalcohólicos</li></a>
<a href="<?php echo "$sitehome/particulares/mascarillas"; ?>" class=""><li>Mascarillas</li></a>
<a href="<?php echo "$sitehome/particulares/accesorios"; ?>" class=""><li>Accesorios</li></a>
<a href="<?php echo "$sitehome/contacto"; ?>"><li>Contáctanos</li></a>
<a href="<?php echo $sitehome; ?>" class=""><li>Volver a la página principal</li></a>
<a href="<?php echo "$sitehome/tienda/carrito/"?>"><li class="carrato">Carrito    <div class="cart-svg"><?php echo carrito(); ?></div></li></a>
<li class="lang-mov"><?php echo do_shortcode('[prisna-google-website-translator]'); ?></li>
</ul>
<?php
// Empresas
elseif ($navbar == 'empresa'): ?>
<ul class="mobile-ulbar-tcs">
<a href="<?php echo "$sitehome/empresas"; ?>" class=""><li>Inicio</li></a>
<a href="<?php echo "$sitehome/empresas/servicio-a-domicilio"; ?>" class=""><li>Servicio a domicilio</li></a>
<a href="<?php echo "$sitehome/empresas/maquinas"; ?>" class=""><li>Máquinas</li></a>
<a href="<?php echo "$sitehome/empresas/antigenos"; ?>" class=""><li>Antígenos</li></a>
<a href="<?php echo "$sitehome/empresas/pcr"; ?>" class=""><li>PCR</li></a>
<a href="<?php echo "$sitehome/empresas/anticuerpos"; ?>" class=""><li>Anticuerpos</li></a>
<a href="<?php echo "$sitehome/contacto"; ?>"><li>Contáctanos</li></a>
<a href="<?php echo "$sitehome/tienda/carrito/"?>"><li class="carrato">Carrito    <div class="cart-svg"><?php echo carrito(); ?></div></li></a>
<a href="<?php echo $sitehome; ?>" class=""><li>Volver a la página principal</li></a>
<li class="lang-mov"><?php echo do_shortcode('[prisna-google-website-translator]'); ?></li>
</ul>
<?php else:
//General
?>
<ul class="mobile-ulbar-tcs">
<a href="<?php echo $sitehome; ?>" class=""><li>Inicio</li></a>
<a href="<?php echo "$sitehome/empresas"; ?>" class=""><li class="empresas-mov">Empresas</li></a>
<a href="<?php echo "$sitehome/particulares"; ?>" class=""><li class="particulares-mov">Particulares</li></a>
<a href="<?php echo "$sitehome/sobre-nosotros"; ?>" class=""><li>Sobre Nosotros</li></a>
<a href="<?php echo "$sitehome/contacto"; ?>"><li>Contáctanos</li></a>
<a href="<?php echo "$sitehome/tienda/carrito/"?>"><li class="carrato">Carrito    <div class="cart-svg"><?php echo carrito(); ?></div></li></a>
<li class="lang-mov"><?php echo do_shortcode('[prisna-google-website-translator]'); ?></li>
</ul>
<?php endif ;?>



        <div id="close-menu" class="close-menu" onclick="closemenu()">X</div>
        </div>
        <div id="menu-togle" class="mobile-toggle" onclick="openmenu()"></div>














    </header>





















</div>
