<?php
$sitehome = get_site_url() ;
?>

<div class="fixed w-full border-b border-gray-300 bg-white shadow z-50">
<?php
if ($navbar == 'particular'): ?>
        <div class="w-full h-10 bg-blue-400 text-white flex items-center justify-center">
            <div>¿Eres una empresa? Dirígete <a href="<?php echo "$sitehome/empresas"; ?>" class="font-bold"> aquí</a>.</div>
        </div>
<?php elseif ($navbar == 'empresa'): ?>
        <div class="w-full h-10 bg-blue-400 text-white flex items-center justify-center">
            <div>¿Eres un particular? Dirígete <a href="<?php echo "$sitehome/particulares"; ?>" class="font-bold"> aquí</a>.</div>
        </div>
      <?php else: ?>
<?php endif ?>
<header class="text-gray-600 body-font">
<div class="container mx-auto flex flex-wrap p-5 flex-col md:flex-row items-center justify-between">
  <span class="flex title-font font-medium items-center justify-center">
    <a href="/"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="Logo" style="max-width: 40px;display: inline;margin-bottom: 6px;"><span class="text-2xl text-blue-900 font-bold"></span></a>
</span>
<?php if ($navbar == 'particular'): ?>
  <a href="<?php echo $sitehome; ?>" class="flex title-font font-medium items-center mb-4 md:mb-0">
      <span title="volver a la página general"
            class="text-2xl text-blue-900 font-bold">TUCOVIDSHOP | &nbsp; <span
              class="text-2xl text-black font-bold">Particulares</span></span>
  </a>
  <nav class="md:ml-auto md:mr-auto flex flex-wrap items-center text-base justify-center">
      <a href="<?php echo "$sitehome/particulares"; ?>" class="mr-5 hover:text-blue-900">Inicio</a>
      <a href="<?php echo "$sitehome/particulares/servicio-domicilio"; ?>" class="mr-5 hover:text-blue-900">Servicio a domicilio</a>
      <a href="<?php echo "$sitehome/particulares/test"; ?>" class="mr-5 hover:text-blue-900">Test</a>
      <a href="<?php echo "$sitehome/particulares/geles-hidroalcoholicos"; ?>" class="mr-5 hover:text-blue-900">Geles Hidroalcohólicos</a>
      <a href="<?php echo "$sitehome/particulares/mascarillas"; ?>" class="mr-5 hover:text-blue-900">Mascarillas</a>
      <a href="<?php echo "$sitehome/particulares/accesorios"; ?>" class="mr-5 hover:text-blue-900">Accesorios</a>
      <a href="<?php echo "$sitehome/particulares/superalimentos"; ?>" class="mr-5 hover:text-blue-900">Superalimentos</a>
  </nav>
<?php
// Empresas
elseif ($navbar == 'empresa'): ?>
<a href="<?php echo $sitehome; ?>" class="flex title-font font-medium items-center mb-4 md:mb-0">
    <span title="volver a la página general"
          class="text-2xl text-blue-900 font-bold">TUCOVIDSHOP | &nbsp; <span
            class="text-2xl text-black font-bold">Empresas</span></span>
</a>
<nav class="md:ml-auto md:mr-auto flex flex-wrap items-center text-base justify-center">
    <a href="<?php echo "$sitehome/empresas"; ?>" class="mr-5 hover:text-blue-900">Inicio</a>
    <a href="<?php echo "$sitehome/empresas/servicio-a-domicilio"; ?>" class="mr-5 hover:text-blue-900">Servicio a domicilio</a>
    <a href="<?php echo "$sitehome/empresas/maquinas"; ?>" class="mr-5 hover:text-blue-900">Máquinas</a>
    <a href="<?php echo "$sitehome/empresas/antigenos"; ?>" class="mr-5 hover:text-blue-900">Antígenos</a>
    <a href="<?php echo "$sitehome/empresas/pcr-nasofaringeos"; ?>" class="mr-5 hover:text-blue-900">PCR Nasofaringeos</a>
    <a href="<?php echo "$sitehome/empresas/pcr-saliva"; ?>" class="mr-5 hover:text-blue-900">PCR Saliva</a>
    <a href="<?php echo "$sitehome/empresas/anticuerpos"; ?>" class="mr-5 hover:text-blue-900">Anticuerpos</a>
</nav>
<?php else:
//General
  ?>
              <a href="<?php echo $sitehome; ?>" class="flex title-font font-medium items-center mb-4 md:mb-0">
                  <span class="text-2xl text-blue-900 font-bold">TUCOVIDSHOP</span>
              </a>
              <nav class="md:ml-auto md:mr-auto flex flex-wrap items-center text-base justify-center">
                  <a href="<?php echo $sitehome; ?>" class="mr-5 hover:text-blue-900">Inicio</a>
                  <a href="<?php echo "$sitehome/empresas"; ?>" class="mr-5 hover:text-blue-900">Empresas</a>
                  <a href="<?php echo "$sitehome/particulares"; ?>" class="mr-5 hover:text-blue-900">Particulares</a>
                  <a href="<?php echo "$sitehome/sobre-nosotros"; ?>" class="mr-5 hover:text-blue-900">Sobre Nosotros</a>
              </nav>
<?php endif ;?>
            <a href="<?php echo "$sitehome/contacto"; ?>"
               class="inline-flex items-center bg-gray-100 border-0 py-2 px-4 focus:outline-none hover:bg-blue-200 rounded text-base mt-4 md:mt-0">
                Contáctanos
                <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                     stroke-width="2" class="w-4 h-4 ml-1" viewBox="0 0 24 24">
                    <path d="M5 12h14M12 5l7 7-7 7"></path>
                </svg>
            </a>
            <div class="g-translator">
            <?php echo do_shortcode('[prisna-google-website-translator]'); ?></div>

        </div>
    </header>


</div>
