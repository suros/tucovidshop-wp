
<?php
//particular
//empresa
$navbar = "";
include 'header.php';

/* Template Name: HOME */;
?>

<x-layout>
    <div class="mb-16 pt-20">
        <div class="bg-blue-200">
            <div class="px-4 py-16 mx-auto sm:max-w-xl md:max-w-full lg:max-w-screen-xl md:px-24 lg:px-8 lg:py-20">
                <div class="max-w-xl mb-10 md:mx-auto sm:text-center lg:max-w-2xl md:mb-12">
                    <div>
                        <h1 class="inline-block px-3 py-px mb-4 text-xs font-semibold tracking-wider text-teal-900 uppercase rounded-full bg-teal-accent-400">
                            TUCOVIDSHOP
                        </h1>
                    </div>
                    <h2 class="max-w-lg mb-6 font-sans text-3xl font-bold leading-none tracking-tight text-gray-900 sm:text-4xl md:mx-auto">
          <span class="relative inline-block">
            <svg viewBox="0 0 52 24" fill="currentColor"
                 class="absolute top-0 left-0 z-0 hidden w-32 -mt-8 -ml-20 text-gray-400 lg:w-32 lg:-ml-28 lg:-mt-10 sm:block">
              <defs>
                <pattern id="dc223fcc-6d72-4ebc-b4ef-abe121034d6e" x="0" y="0" width=".135" height=".30">
                  <circle cx="1" cy="1" r=".7"></circle>
                </pattern>
              </defs>
              <rect fill="url(#dc223fcc-6d72-4ebc-b4ef-abe121034d6e)" width="52" height="24"></rect>
            </svg>
            <span class="relative">Todo lo</span>
                        </span>
                        necesario para ayudarte a protegerte contra la Covid-19
                    </h2>
                    <p class="text-base text-gray-700 md:text-lg">

<?php echo the_field('introduccion')?>
                    </p>
                </div>
                <div class="flex items-center sm:justify-center">
                  <a href="<?php echo "$sitehome/empresas"; ?>" aria-label=""
                    class="inline-flex items-center justify-center h-12 px-6 mr-6 font-medium tracking-wide text-white transition duration-200 rounded shadow-md bg-blue-400 buttonempresa hover:bg-blue-700 focus:shadow-outline focus:outline-none">
                    Soy empresa
                  </a>
                    <a href="<?php echo "$sitehome/particulares"; ?>"
                       class="inline-flex items-center justify-center h-12 px-6 mr-6 font-medium tracking-wide text-white transition duration-200 rounded shadow-md bg-blue-400 buttonparticulares hover:bg-blue-700 focus:shadow-outline focus:outline-none">
                        Soy particular
                    </a>
                </div>
            </div>
        </div>
        <div class="relative px-4 sm:px-0">
            <div class="absolute inset-0 bg-blue-200 h-1/2"></div>
            <div
                class="relative grid mx-auto overflow-hidden bg-white divide-y rounded shadow sm:divide-y-0 sm:divide-x sm:max-w-screen-sm sm:grid-cols-3 lg:max-w-screen-md">
                <div class="inline-block p-8 text-center">
                    <div class="flex items-center justify-center w-12 h-12 mx-auto mb-4 rounded-full bg-indigo-50">
                        <svg class="w-10 h-10 text-deep-blue-accent-400" stroke="currentColor" viewBox="0 0 52 52">
                            <polygon stroke-width="3" stroke-linecap="round" stroke-linejoin="round" fill="none"
                                     points="29 13 14 29 25 29 23 39 38 23 27 23"></polygon>
                        </svg>
                    </div>
                    <p class="font-bold tracking-wide text-gray-800"><?php echo the_field('boton1')?></p>
                </div>
                <div class="inline-block p-8 text-center">
                    <div class="flex items-center justify-center w-12 h-12 mx-auto mb-4 rounded-full bg-indigo-50">
                        <svg class="w-10 h-10 text-deep-blue-accent-400" stroke="currentColor" viewBox="0 0 52 52">
                            <polygon stroke-width="3" stroke-linecap="round" stroke-linejoin="round" fill="none"
                                     points="29 13 14 29 25 29 23 39 38 23 27 23"></polygon>
                        </svg>
                    </div>
                    <p class="font-bold tracking-wide text-gray-800"><?php echo the_field('boton2')?></p>
                </div>
                <div class="inline-block p-8 text-center">
                    <div class="flex items-center justify-center w-12 h-12 mx-auto mb-4 rounded-full bg-indigo-50">
                        <svg class="w-10 h-10 text-deep-blue-accent-400" stroke="currentColor" viewBox="0 0 52 52">
                            <polygon stroke-width="3" stroke-linecap="round" stroke-linejoin="round" fill="none"
                                     points="29 13 14 29 25 29 23 39 38 23 27 23"></polygon>
                        </svg>
                    </div>
                    <p class="font-bold tracking-wide text-gray-800"><?php echo the_field('boton3')?></p>
                </div>
            </div>
        </div>
    </div>

    <section class="text-gray-600 body-font">
        <div class="container px-5 py-12 mx-auto">
            <div class="flex flex-col text-center w-full mb-10">
                <h1 class="sm:text-3xl text-2xl font-medium title-font mb-4 text-gray-900">Entiende el virus</h1>
                <p class="lg:w-2/3 mx-auto leading-relaxed text-base">La pandemia del COVID-19 ha parado nuestras vidas y cambiado nuestra sociedad, pero no todo es negativo. Hoy gracias a la ciencia y a la innovación estamos más cerca que nunca de parar esta pandemia. Todos juntos. Por ello hay que ver también los datos positivos.
</p>
            </div>
            <div class="flex flex-wrap -m-4 text-center">
                <div class="p-4 md:w-1/3 w-full">
                    <div class="border-2 border-gray-200 px-4 py-6 rounded-lg">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/syringe.svg"
                             class="text-indigo-500 w-12 h-12 mb-3 inline-block"
                             alt="">

                        <h2 class="title-font font-medium text-3xl text-gray-900">200M</h2>
                        <p class="leading-relaxed">De dosis administradas</p>
                    </div>
                </div>
                <div class="p-4 md:w-1/3 w-full">
                    <div class="border-2 border-gray-200 px-4 py-6 rounded-lg">
                        <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                             stroke-width="2" class="text-blue-400 w-12 h-12 mb-3 inline-block" viewBox="0 0 24 24">
                            <path d="M17 21v-2a4 4 0 00-4-4H5a4 4 0 00-4 4v2"></path>
                            <circle cx="9" cy="7" r="4"></circle>
                            <path d="M23 21v-2a4 4 0 00-3-3.87m-4-12a4 4 0 010 7.75"></path>
                        </svg>
                        <h2 class="title-font font-medium text-3xl text-gray-900">127M</h2>
                        <p class="leading-relaxed">De test negativos</p>
                    </div>
                </div>
                <div class="p-4 md:w-1/3 w-full">
                    <div class="border-2 border-gray-200 px-4 py-6 rounded-lg">
                        <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"
                             stroke-width="2" class="text-blue-400 w-12 h-12 mb-3 inline-block" viewBox="0 0 24 24">
                            <path d="M17 21v-2a4 4 0 00-4-4H5a4 4 0 00-4 4v2"></path>
                            <circle cx="9" cy="7" r="4"></circle>
                            <path d="M23 21v-2a4 4 0 00-3-3.87m-4-12a4 4 0 010 7.75"></path>
                        </svg>
                        <h2 class="title-font font-medium text-3xl text-gray-900">77M</h2>
                        <p class="leading-relaxed">De curados</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

</x-layout>




<?php get_footer(); ?>
