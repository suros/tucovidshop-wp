
<div itemscope itemtype="http://schema.org/Product" id="product-<?php the_ID(); ?>" <?php post_class(); ?>>
<div class="row">
<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
<?php
/**
* woocommerce_show_product_images hook
*
* @hooked woocommerce_show_product_sale_flash - 10
* @hooked woocommerce_show_product_images - 20
*/
do_action( 'woocommerce_before_single_product_summary' );
?>
</div>
<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
<div class="summary entry-summary">

<?php
/**
* woocommerce_single_product_summary hook
*
* @hooked woocommerce_template_single_title - 5
* @hooked woocommerce_template_single_price - 10
* @hooked woocommerce_template_single_excerpt - 20
* @hooked woocommerce_template_single_add_to_cart - 30
* @hooked woocommerce_template_single_meta - 40
* @hooked woocommerce_template_single_sharing - 50
*/
do_action( 'woocommerce_single_product_summary' );
?>
</div><!-- .summary -->
</div>
</div>

<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<?php
/**
* woocommerce_after_single_product_summary hook
*
* @hooked woocommerce_output_product_data_tabs - 10
* @hooked woocommerce_output_related_products - 20
*/
do_action( 'woocommerce_after_single_product_summary' );
?>
</div>
</div>
</div><!-- #product-<?php the_ID(); ?> -->

<?php do_action( 'woocommerce_after_single_product' ); ?>
