<?php
//particular
//empresa
$navbar = "";
include 'header.php';
?>
<style>
.center404 {
    margin: auto;
    width: 50%;
    text-align: center;
    margin-top: -2em;
}
img.cuatrocerocuatro {
    width: 300px;
    margin: auto;
}
.ps-1.text-blue-900 {
    font-size: 100px;
    font-weight: 900;
        margin-top: -10px;
}
.ps-2.text-blue-900 {
    font-size: 20px;
    font-weight: 700;
}
.ps-3.text-blue-900 {
    font-size: 18px;
}
.forty{
  margin-bottom: 14px;
}
</style>
<x-layout>
<div>
<div class="bg-white pt-8">
<main>
<div class="pt-12 sm:pt-16 lg:pt-20">

<div class="center404">
<h1 class="text-3xl font-extrabold tracking-tight text-gray-900 sm:text-4xl forty">Error 404</h1>

<img src="<?php echo get_template_directory_uri(); ?>/images/pingomasc.png"
     class="cuatrocerocuatro"
     alt="">
<div class="ps-1 text-blue-900">¡Ups!</div>

<div class="ps-2 text-blue-900">¿Has oído hablar del error 404? Pasa cuando
el enlace de una página no existe</div>

<div class="ps-3 text-blue-900">En TUCOVIDSHOP creemos que nunca te irás a
dormir sin saber algo nuevo.</div>

</div>
</div>
</main>
</div>

</div>
</x-layout>


<?php get_footer(); ?>
