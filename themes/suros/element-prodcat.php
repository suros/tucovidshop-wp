<?php add_action('wp_head', 'get_current_product_category');

 ?>



 <div class="lg:w-1/4 md:w-1/2 p-4 w-full rounded hover:shadow-xl transition duration-300">
   <a href="<?php
   //enlace del producto
   echo get_permalink(); ?>">
             <div class="block relative h-48 rounded overflow-hidden"
              >

                        <img alt="ecommerce" class="object-cover object-center w-full h-full block"
                             src="<?php echo
                             //url de la imagen
                             get_the_post_thumbnail_url();
                             // get_the_post_thumbnail(); ?>">
  </div>
                    <div class="mt-4">
                        <h3 class="text-gray-500 text-xs tracking-widest title-font mb-1"><?php // echo get_current_product_category(); ?></h3>
                        <h2 class="text-gray-900 title-font text-lg font-medium"><?php echo the_title(); ?></h2>
                        <?php
                        // texto corto
                        the_excerpt(); ?>

                        <p class="mt-1"><?php echo add_price_widget();?>€</p>
                    </div>

                  </a>
              </div>
