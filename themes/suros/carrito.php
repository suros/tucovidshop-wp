<?php
include 'header.php';
?>
<?php /* Template Name: carrito */;?>
<x-layout>
<div>
<div class="bg-white pt-8">
<main>
<div class="pt-12 sm:pt-16 lg:pt-20">
<?php echo titleh1(); ?>

<?php
echo get_the_content();
do_shortcode('[woocommerce_cart]');
?>



</div>
</main>
</div>

</div>
</x-layout>

<?php get_footer(); ?>
