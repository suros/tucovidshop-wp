<?php
include 'head.php';
?>
<?php /* Template Name: e-domicilio */;?>
<x-layout>
<div>
<div class="bg-white pt-8">
<main>
<div class="pt-12 sm:pt-16 lg:pt-20">


<?php // headline 1 general ?>
<?php echo titleh1(); ?>



<div class="relative max-w-xl mx-auto">
<?php
echo do_shortcode('[booking type=1 nummonths=2]');
?>
</div>


</div>
</main>
</div>

</div>
</x-layout>

<?php get_footer(); ?>
