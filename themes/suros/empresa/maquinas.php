<?php
include 'head.php';
?>
<?php /* Template Name: e-maquinas */;?>

<x-layout>
    <section class="text-gray-600 body-font py-12">
              <div class="container px-5 py-24 mx-auto">
                <?php echo titleh1(); ?>
        <div class="flex flex-wrap justify-around">
<?php
$args = array(
    'posts_per_page' => -1,
    //'post__not_in' => array($post->ID), // Ensure that the current post is not displayed
    'product_cat' => 'maquinas',
    'post_type' => 'product',
    'orderby' => 'title',
    'order'   => 'DESC',
  //  'orderby' => 'meta_value', // rand Randomize the results, date date
  //  'meta_key'=> 'start_date',
);
$the_query = new WP_Query( $args );
// The Loop
while ( $the_query->have_posts() ) {
    $the_query->the_post();
get_template_part('element', 'prodcat');

}
wp_reset_postdata();
?>

</div>
        </div>
<div class="filtro">
<?php echo do_shortcode('[wpf-filters id=1]'); ?>
</div>

</section>
</x-layout>
<?php get_footer(); ?>
