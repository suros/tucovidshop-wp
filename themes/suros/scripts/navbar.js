//expand-menu
function openmenu() {
document.getElementById("mobile-nab").className += " expand-menu";
document.getElementById("menu-togle").setAttribute( "onClick", "closemenu()" );
}

function closemenu() {
document.getElementById("mobile-nab").className =document.getElementById("mobile-nab").className.replace( /(?:^|\s)expand-menu(?!\S)/g , '' );
document.getElementById("menu-togle").setAttribute( "onClick", "openmenu()" );
}

jQuery(document).ready(function(){
	/* OPAQUE HEADER & PARALLAX */
	cpotheme_update_sticky_header();
	var slider_height = jQuery('.slider-slides').height();
	var fadeUntil = slider_height, paddingFactor = slider_height / 3;
	jQuery(window).bind('scroll', function(){
		cpotheme_update_sticky_header();
		var offset = jQuery(document).scrollTop(), padding = 0;
		if(offset <= fadeUntil){
			padding = paddingFactor * offset / fadeUntil;
			jQuery('.slide').css('background-position', 'center ' + padding + 'px');
		}
	});
});

function cpotheme_update_sticky_header(){
    if(jQuery(document).scrollTop() > 15)
		jQuery('.supernavbar').addClass('header-sticky-active');
	else
		jQuery('.supernavbar').removeClass('header-sticky-active');
}
