<?php
//particular
//empresa
$navbar = "";
include 'header.php';
?>
<?php /* Template Name: sobrenosotros */;?>

<x-layout>
  <div class="mb-16 pt-20">
      <div class="bg-blue-200">
          <div class="px-4 py-16 mx-auto sm:max-w-xl md:max-w-full lg:max-w-screen-xl md:px-24 lg:px-8 lg:py-20">
              <div class="max-w-xl mb-10 md:mx-auto sm:text-center lg:max-w-2xl md:mb-12">
                  <div>
                      <span class="inline-block px-3 py-px mb-4 text-xs font-semibold tracking-wider text-teal-900 uppercase rounded-full bg-teal-accent-400">
                          TUCOVIDSHOP
                      </span>
                  </div>
                  <span class="max-w-lg mb-6 font-sans text-3xl font-bold leading-none tracking-tight text-gray-900 sm:text-4xl md:mx-auto">
        <span class="relative inline-block">
          <svg viewBox="0 0 52 24" fill="currentColor"
               class="absolute top-0 left-0 z-0 hidden w-32 -mt-8 -ml-20 text-gray-400 lg:w-32 lg:-ml-28 lg:-mt-10 sm:block">
            <defs>
              <pattern id="dc223fcc-6d72-4ebc-b4ef-abe121034d6e" x="0" y="0" width=".135" height=".30">
                <circle cx="1" cy="1" r=".7"></circle>
              </pattern>
            </defs>
            <rect fill="url(#dc223fcc-6d72-4ebc-b4ef-abe121034d6e)" width="52" height="24"></rect>
          </svg>
          <h1 class="relative">Sobre Nosotros</h1>
                  <p class="text-base text-gray-700 md:text-lg">

                  </p>
              </div>
          </div>
      </div>
      <div class="noventayseis"></div>
          <div class="text-center">
              <h2 class="mt-4 text-3xl font-extrabold tracking-tight text-gray-900 sm:text-4xl">
              <?php echo the_field('titulo-1-about');?>
              </h2>
              <p class="mt-4 text-lg leading-6 text-gray-500">
              <?php echo the_field('texto-1-about');?>
              </p>
          </div>
          <!-- Testimonial/stats section -->
          <div class="relative mt-20">
          <div class="lg:mx-auto lg:max-w-7xl lg:px-8 lg:grid lg:grid-cols-2 lg:gap-24 lg:items-start">
          <div class="relative sm:py-16 lg:py-0">
          <div aria-hidden="true"
          class="hidden sm:block lg:absolute lg:inset-y-0 lg:right-0 lg:w-screen">
          <div
          class="absolute inset-y-0 right-1/2 w-full bg-gray-50 rounded-r-3xl lg:right-72"></div>
          <svg class="absolute top-8 left-1/2 -ml-3 lg:-right-8 lg:left-auto lg:top-12"
          width="404" height="392" fill="none" viewBox="0 0 404 392">
          <defs>
          <pattern id="02f20b47-fd69-4224-a62a-4c9de5c763f7" x="0" y="0" width="20"
          height="20" patternUnits="userSpaceOnUse">
          <rect x="0" y="0" width="4" height="4" class="text-gray-200"
          fill="currentColor"></rect>
          </pattern>
          </defs>
          <rect width="404" height="392"
          fill="url(#02f20b47-fd69-4224-a62a-4c9de5c763f7)"></rect>
          </svg>
          </div>


          <div
          class="relative mx-auto max-w-md px-4 sm:max-w-3xl sm:px-6 lg:px-0 lg:max-w-none lg:py-20">
          <!-- Testimonial card-->
          <div class="relative pt-64 pb-10 rounded-2xl shadow-xl overflow-hidden">
          <img class="absolute inset-0 h-full w-full object-cover"
          src="<?php echo the_field('image-about');?>"
          alt="">

          <div class="absolute inset-0 bg-rose-500" style="mix-blend-mode: multiply;"></div>
          <div
          class="absolute inset-0 bg-gradient-to-t from-rose-600 via-rose-600 opacity-90"></div>
          <div class="relative px-8">
          <div>

          </div>
          <blockquote class="mt-8">
          <div class="relative text-lg font-medium text-white md:flex-grow">
          <svg
          class="absolute top-0 left-0 transform -translate-x-3 -translate-y-2 h-8 w-8 text-rose-400"
          fill="currentColor" viewBox="0 0 32 32" aria-hidden="true">
          <path
          d="M9.352 4C4.456 7.456 1 13.12 1 19.36c0 5.088 3.072 8.064 6.624 8.064 3.36 0 5.856-2.688 5.856-5.856 0-3.168-2.208-5.472-5.088-5.472-.576 0-1.344.096-1.536.192.48-3.264 3.552-7.104 6.624-9.024L9.352 4zm16.512 0c-4.8 3.456-8.256 9.12-8.256 15.36 0 5.088 3.072 8.064 6.624 8.064 3.264 0 5.856-2.688 5.856-5.856 0-3.168-2.304-5.472-5.184-5.472-.576 0-1.248.096-1.44.192.48-3.264 3.456-7.104 6.528-9.024L25.864 4z"></path>
          </svg>

          </div>


          </blockquote>
          </div>
          </div>
          </div>
          </div>

          <div class="relative mx-auto max-w-md px-4 sm:max-w-3xl sm:px-6 lg:px-0">
          <!-- Content area -->
          <div class="pt-12 sm:pt-16 lg:pt-20">
          <h1 class="text-3xl text-gray-900 font-extrabold tracking-tight sm:text-4xl">
            <p>
            <?php echo the_field('titulo-2-about');?>
            </p>
          </h1>
          <div class="mt-6 text-gray-500 space-y-6">
            <p>
            <?php echo the_field('texto-2-about');?>
            </p>
          </div>
          </div>

          <!-- Stats section -->

          </div>
          </div>
          </div>

      </div>
</x-layout>
<?php get_footer(); ?>
