<?php
$sitehome = get_site_url() ;
// Permanent 301 redirection
header("HTTP/1.1 301 Moved Permanently");
header("location: $sitehome");
exit();
?>
